
var z =  new Tone.DrumSynth(drum_presets,4); 
z.rvb = new Tone.Freeverb();
z.chain(z.rvb, Tone.Master);

z.rvb.set({"wet" : 0.5, "roomSize" : 0.2, "dampening" : 5000});




var ldur = "14m";
var l = "16n";
var v = 7;
var a = `
	@BPM:130
	@L:4n
	X	X	X	X
	@L:8t
	KR3 R1 R2    SR52   R7  KR43         SR42     R7
 	KR43         SR42   R7  KR42   S11   SR4  R7  B4
  	KC62   H     SH32   H   KH52   H     SH32     H
  	KH6 H1 H1    SH32   H   KH52   SH    T3O6 TH1 FH
	@L:16n
	KC72   H42   SH72    H42     KH72    K4H42  SH72    H42  
	KH72   H42   SH72    H42     KH72    KSH4 S T    T  F  R 
	KR64         SHR43       R8  KR64           SHR4 R3 R5 R8 
	KR64         SHR43       R8  KB63         B SHR42   B52  
	KC42   H72   KSO42   H72     KO42    H72    KSO42   H72  
	KO42   H72   KSO42   H72     KO42    H72    KSO42   H72  
	KH22   H12   H32     H12     XH52    H12    H12     KH52  
	KH22   H12   KH32    H12     XH52    H12    H12     O52   
	KS1 S1 S  S1 S1H  S  S1  S1  KS  S2  S3 S   S4H S5  S   S6
`;


/*
var ldur = "7m";
var l = "8n";
var v = 7;
var a = `
	K2H Z H  Z  K2H H1 H  H3  K2H H5 O   Z   K2H H O   H3
	K2R Z HR Z  K2R R1 HR R3  K2R R5 HRB R7  K2R R HRB R
	K2C Z S2R Z K2R C1 S2R C3 K2R C5 S2R C7  K2C C S2RBC Z

	@FINE
`;
*/


var nts = notation_parse(a, v, l );


var seq = new Tone.Part(function(time, e){
	//var e = nts[i];
	console.log(time, e);
	if(e.note){
		z.triggerAttackRelease(e.note, e.dur, time, e.vel);
	}
	if(e.tag){
		if(e.tag == "BPM"){ Tone.Transport.bpm.value = e.val; }
		if(e.tag == "L"){   l = Tone.Time(e.val).toSeconds(); }
		if(e.tag == "FINE"){ stop(); }
	}
});

nts.forEach( function(e){ seq.add(e.time, e) } );

seq.loop = true;
seq.loopEnd = ldur;
seq.humanize = true;
seq.start();


function start(){
	stop();
	Tone.Transport.start();
}
function stop(){
	Tone.Transport.stop();
}

//start()

