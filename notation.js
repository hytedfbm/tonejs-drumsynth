
// params:
//		a = notaion string
//				notes played together
//				ie KHS23
//					notes K and H, vel = (2/10), dur = (3*L)
//				ie Z
//					rest, dur = (1*L)
//		v1 = default vel (ie 1--10)
//		l1 = (ie 8n, 8t...) 
function notation_parse(a, v1, l1){
	var b = a.split(/\s+|\|+/);
	var events = [];
	var t = 0;
	var l = Tone.Time(l1).toSeconds();

	b.forEach(function(d){
		var e;

		// @TAG:VAL
		if(e = d.match(/\@(\w+)\:?(.*)/)){
			var tag = e[1];
			var val = e[2];

			//if(tag == "BPM"){ Tone.Transport.bpm.value = val; }
			//if(tag == "L"){   l = Tone.Time(val).toSeconds(); }

			var m = {"time" : t, "tag" : tag, "val" : val}
			events.push(m);
		}
		// NOTES..., velocity, dur
		else if(e = d.match(/[zZ]|[a-yA-Y]\d?|[\/\d]+/g)){
			var ee = notation_dur(e);
			e = ee[0];
			dur = ee[1] * l;

			e.forEach(function(f){  // \w+\d?
				var vel = v1;
				var g = f.match(/./g);

				if(g[g.length-1].match(/^\d+$/)){ vel = g.pop(); }
				if(vel == 0){ vel = 10; }
				vel = Number(vel) / 10;

				g.forEach(function(h){ //each letter
					var m = {"time" : t, "note" : h, "vel" : vel, "dur" : dur};
					events.push(m);
				});
			});

			t += dur;
		}

	});
	
	return events;
}

/////////////////////////
function notation_dur(e){
	dur = 1;

	var last = e[e.length-1];
	if(last.match(/^\d+$/)){	dur = e.pop(); }
	else if(last == "/"){ e.pop(); dur = 0.5; }
	else if(last == "//"){ e.pop(); dur = 0.25; }
	else if(last == "/3"){ e.pop(); dur = 1/3; }
	else if(last == "2/3"){ e.pop(); dur = 2/3; }

	return [e, dur];
}
