Tone.MembraneSynth.prototype.trig = function(pitch, settings, time, vel){
	this.set(settings);
	this.triggerAttack(pitch, time, vel);
	return this;
}
Tone.NoiseSynth.prototype.trig = function(pitch, settings, time, vel){
	this.set(settings);
	this.triggerAttack(time, vel);
	return this;
}
Tone.MetalSynth.prototype.trig = function(pitch, settings, time, vel){
	this.set(settings);
	this.triggerAttack(time, vel);
	return this;
}


//////////////////////////////////////////////////
Tone.DrumSynth = function(presets, poly, options){

	//get the defaults
	options = Tone.defaultArg(options, Tone.DrumSynth.defaults);
	Tone.Instrument.call(this, options);

	//setup key mapping
	this.poly = poly;
	this.map = [];
	this.links = [];

	//var presets = options.presets;
	for(var i in presets){
		var p = presets[i];



		for(var j in p.map){
			var k = p.map[j];

			if(! this.map[k]){ this.map[k] = []; }
			this.map[k].push(p);

			if(p.link){
				if(! this.links[p.link]){ this.links[p.link] = []; }
				this.links[p.link].push(k);
			}
		}
	}

	//setup voices
	this.voices = [];
	this.create_voice("NoiseSynth", 1);
	this.create_voice("MetalSynth", poly);
	this.create_voice("MembraneSynth", poly);

	this.notes_active = [];
	this.active_link = [];

	this._readOnly(["poly", "map", "voices"]);
};

Tone.extend(Tone.DrumSynth, Tone.Instrument);

Tone.DrumSynth.defaults = {};

/////////////////////////////////////
Tone.DrumSynth.prototype.create_voice = function(synth, poly){
	var voices = new Array(poly);
	var vvv = {'v' : voices, 'idx' : -1, 'poly' : poly};

	for(var i=0; i<poly; i++){
		var v = new Tone[synth]();
		v.connect(this.output);
		vvv.v[i] = v;
	}

	this.voices[synth] = vvv;

	return vvv;
}

/////////////////////////////////////
function proc_dynamics(vel, snode, dnode){
	var n2 = Object.assign({}, dnode, snode);

	for(var p in dnode){
			var val = dnode[p];

			if(Array.isArray(val)){ //calculated value
				n2[p] = vel * (val[1] - val[0]) + val[0];
			}
			else if(typeof val == "object"){ //recursive copy
				n2[p] = proc_dynamics(vel, n2[p], val);
			}
		}
	return n2;
}

Tone.DrumSynth.prototype.nextVoice = function(synth){
	var v;
	var voices = this.voices[synth];

	/////voice.getLevelAtTime(time) > 1e-5
	if(voices){
		var idx = ++voices.idx;
		if(idx >= voices.poly){ idx = voices.idx = 0; }
		v = voices.v[idx];
	}
	return v;
}

/////////////////////////////////////
Tone.DrumSynth.prototype.triggerAttack = function(note, time, velocity){
	time = this.toSeconds(time);
	var presets = this.map[note];

	var link_grp = presets[0].link;
console.log('attack note='+note, 'link='+link_grp);
	if(link_grp){
//		this.triggerQRelease(link_grp, '+0');
//		for(var i in this.links[link_grp]){
//			this.triggerQRelease(this.links[link_grp][i], '+0');
//		}
	}
	if(this.notes_active[note]){
		this.triggerRelease(note, '+0');
	}
	var active = this.notes_active[note] = [];


	for(var i in presets){
		var p = presets[i];

		var vel = velocity ? velocity : 1;
		var settings = proc_dynamics(vel, p.settings, p.pf);
		if(p.vol){ vel *= p.vol; }

		var v = this.nextVoice(p.synth);
		active.push(v);

		v.trig(p.pitch, settings, time, vel);

		if(link_grp){
			if(! this.active_link[link_grp]){ this.active_link[link_grp] = []; }
			this.active_link[link_grp].push(v);
		}


		// doesnt really work quite as well expected
/*		if(p.link){
			var re_v = this.active_link[p.link];
			if(re_v){
console.log("release", p.link);
				var mute = 0.00001;
				re_v.set({ "envelope" : {"attack" : mute , "decay" : mute, "release" : mute} });
				this.triggerRelease(re_v, time);
			}
			this.active_link[p.link] = v;
		}
*/

	}

	return this;
};

////////////////
Tone.DrumSynth.prototype.triggerRelease = function(note, time){
	//var presets = this.map[note];
	time = this.toSeconds(time);

	var active = this.notes_active[note];
	if(active){
console.log('releasing note ' + note);
		for(var i in active){
			var v = active[i];
			v.triggerRelease(time);
		}
		this.notes_active[note] = undefined;
	}

	return this;
};
/*Tone.DrumSynth.prototype.triggerQRelease = function(link_grp, time){
	//var presets = this.map[note];
	time = this.toSeconds(time);
	var mute = 0.00001;

	var active = this.active_link[link_grp];
console.log('  qrel .. ' + link_grp );
	if(active){
console.log('quick releasing link group ' + link_grp);
		for(var i in active){
			var v = active[i];
			v.set({ "envelope" : {"attack" : mute , "decay" : mute, "release" : mute} });
			v.triggerRelease(time);
		}
		this.active_links[link_grp] = undefined;
	}

	return this;
};*/


Tone.DrumSynth.prototype.triggerAttackRelease = function(note, duration, time, velocity){
	if(time == null){ time = "+0"; }
	duration = this.toSeconds(duration);
	this.triggerAttack(note, time, velocity);
	this.triggerRelease(note, time + duration);
	return this;
};

Tone.DrumSynth.prototype.dispose = function(){
	Tone.Instrument.prototype.dispose.call(this);
	this._writable(["poly", "map", "voices"]);

	this.poly = null;
	this.map = null;

	for(var v1 in voices){
		var v2 = this.voices[v1];
		for(var i=0; i<v2.poly; i++){
			v2.v[i].dispose();
			v2.v[i] = null;
		}
	}

	return this;
};




