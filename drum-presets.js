var drum_presets = [
{
	"name" : "kick",
	"synth" : "MembraneSynth",
	"pitch" : "80hz",
	"map" : [35,36,'K','KD'],

	"settings" : {
		"pitchDecay"  : 0.05,
		"octaves" : 3, 
		"oscillator"  : { "type"  : "sine" },
		"envelope" : { 
			"attack"  : 0.001,
			"decay" : 0.2,
			"release"  : 1.4 ,
			"attackCurve"  : "exponential"
		} 
	}
},

{
	"name" : "Tom",
	"synth" : "MembraneSynth",
	"pitch" : "200hz",
	"map" : [47,48,50,'T','TM','MT','HT'],

	"settings" : {
		"pitchDecay"  : 0.01 ,
		"octaves" : 1.2, 
		"oscillator"  : { "type"  : "sine" },
		"envelope" : {
			"attack"  : 0.002 ,
			"decay" : 1.4,
			"sustain"  : 0.0 ,
			"release"  : 1.4 ,
			"attackCurve"  : "exponential"
		} 
	}
},

{
	"name" : "FloorTom",
	"synth" : "MembraneSynth",
	"pitch" : "145hz",
	"vol" : 0.8,
	"map" : [41,43,45,'F','FT'],

	"settings" : {
		"pitchDecay"  : 0.04 ,
		"octaves" : 1.6, 
		"oscillator"  : { "type"  : "sine" },
		"envelope" : {
			"attack"  : 0.05 ,
			"decay" : 1.2,
			"sustain"  : 0.0 ,
			"release"  : 1.2 ,
			"attackCurve"  : "exponential"
		} 
	}
},

{
	"name" : "Snare-CrossStick-Layer1",
	"synth" : "MembraneSynth",
	"pitch" : "420hz",
	"vol" : 0.4,
	"map" : [37,39,'X', 38,40,"S","SN"],

	"settings" : {
		"pitchDecay"  : 0.01 ,
		"octaves"  : 2 ,
		"oscillator"  : { "type"  : "square2" }  ,
		"envelope"  : {
			"attack"  : 0.001 ,
			"decay"  : 0.15 ,
			"sustain"  : 0.0 ,
			"release"  : 0.15 ,
			"attackCurve"  : "exponential"
		}
	}
},
{
	"name" : "CrossStick-Layer2",
	"synth" : "NoiseSynth",
	"vol" : 1,
	"map" : [37,39,'X'],

	"settings" : {
		"noise"  : { "type"  : "brown" },
		"envelope" : { 
			"attack" : 0.005,
			"decay" : 0.1,
			"sustain" : 0
		}
	}
},
{
	"name" : "Snare-Wire-Layer2",
	"synth" : "NoiseSynth",
	"vol" : 2.3,
	"map" : [38,40,"S","SN"],

	"settings" : {
		"noise"  : { "type"  : "brown" },
		"envelope" : { 
			"attack" : 0.01, 
			"decay" : 0.3,
			"sustain" : 0
		}
	},
	"pf" : {
		"envelope" : { 
			"attack" : [0.03, 0.02],
			"decay" : [0.25, 0.35]
		}
	}
},

{
	"name" : "Hi-Hat",
	"synth" : "MetalSynth",
	"vol" : 0.03,
	"map" : [42,44,"H","HH","HP"],
	"link" : "HH",

	"settings" : {
		"frequency" : 150, 
		"octaves" : 3.7,
		"harmonicity" : 3.09, 
		"modulationIndex" : 33, 
		"resonance" : 4000, 
		"envelope" : { 
			"attack" : 0.001,
			"decay" : 0.3,
			"release" : 0.01,
		}
	},
	"pf" : {
		"modulationIndex" : [34,33],
		"resonance" : [4000,6000],
		"envelope" : { 
			"attack" : [0.01, 0.001],
			"decay" : [0.4, 0.2]
		}
	}
},
{
	"name" : "Hi-Open",
	"synth" : "MetalSynth",
	"vol" : 0.07,
	"map" : [46,"O","HO"],
	"link" : "HH",

	"settings" : {
		"frequency" : 150, 
		"octaves" : 3.7,
		"harmonicity" : 3.09, 
		"modulationIndex" : 33, 
		"resonance" : 3500, 
		"envelope" : { 
			"attack" : 0.01,
			"decay" : 1.2,
			"release" : 1.1,
		}
	},
	"pf" : {
		"modulationIndex" : [34,33],
		"resonance" : [3500,5500],

		"envelope" : { 
			"attack" : [0.015, 0.01],
			"decay" : [2.0, 1.2]
		}
	}
},

{
	"name" : "Ride",
	"synth" : "MetalSynth",
	"vol" : 0.02,
	"map" : [51,59,"R","RC"],
	"link" : "RIDE",

	"settings" : {
		"frequency" : 50,
		"octaves" : 1.5,
		"harmonicity" : 6.3, 
		"modulationIndex" : 151, 
		"resonance" : 2300, 
		"envelope" : { 
			"attack": 0.01, 
			"decay" : 2.2, 
			"release" : 2.2 
		} 
	},
	"pf" : {
		"modulationIndex" : [140,160],
		"resonance" : [2800,1300],
		"envelope" : { 
			"attack" : [0.02, 0.0001],
			"decay" : [2, 2.4],
			"release" : [2, 2.4]
		}
	}
},
{
	"name" : "Ride-Bell",
	"synth" : "MetalSynth",
	"vol" : 0.03,
	"map" : [53,"B","RBEL", "RB"],
	"link" : "RIDE",

	"settings" : {
		"frequency" : 50,
		"octaves" : 1.5,
		"harmonicity" : 6.01, 
		"modulationIndex" : 80, 
		"resonance" : 1300, 
		"envelope" : { 
			"attack": 0.01, 
			"decay" : 1.2, 
			"release" : 1.2 
		}
	},
	"pf" : {
		"modulationIndex" : [80,10],
		"envelope" : { 
			"attack" : [0.01, 0.001],
			"decay" : [1.2, 1.4],
			"release" : [1.2, 1.4]
		}
	}
},

{
	"name" : "Crash",
	"synth" : "MetalSynth",
	"vol" : 0.5,
	"map" : [49,52,55,57,"C","CC"],

	"settings" : {
		"frequency" : 400,
		"octaves" : 3.5,
		"harmonicity" : 5.1, 
		"modulationIndex" : 70, 
		"resonance" : 2500, 
		"envelope" : { 
			"attack": 0.01, 
			"decay" : 1.0, 
			"release" : 1.0
		} 
	},
	"pf" : {
		"modulationIndex" : [60, 100],
		"envelope" : { 
			"attack" : [0.01, 0.00001],
			"decay" : [1.0, 2.7],
			"release" : [1.0, 2.7]
		}
	}
},

];
